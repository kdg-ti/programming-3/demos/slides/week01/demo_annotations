package be.kdg.programming3;

import be.kdg.programming3.annotations.Heavy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class StartApplication {
    public static void main(String[] args) {
        for (Method method: ClassWithHeavyMethods.class.getDeclaredMethods()) {
            Heavy heavy = method.getAnnotation(Heavy.class);
            if (heavy!=null) {
                System.out.println("Found heavy method:" + method.getName());
                try {
                    long time = System.currentTimeMillis();
                    method.invoke(null);
                    long deltaTime = System.currentTimeMillis() - time;
                    if (deltaTime>heavy.maxTime()) {
                        System.out.println("The method was to slow!");
                        System.out.printf("It took %d ms, while maxtime was %d ms!\n", deltaTime, heavy.maxTime());
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
